//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Certificate is Ownable,ERC721 {

    
   
    uint[] public serielNumbers;
    mapping(uint => bool) _serielExists;



    constructor() ERC721("Certificate","CERTIFICATE") public {
       
    }
    
   


    function certify (uint _serielNumber , address _certifiedUser, string memory _uri) public onlyOwner{
        require(!_serielExists[_serielNumber],"Number already given");
        serielNumbers.push(_serielNumber);
        
        address owned = _certifiedUser;
        _mint(owned,_serielNumber);

    

        _serielExists[_serielNumber]=true;
        
    }

    function issue (address _certifiedUser,uint _serielNumber  ) public {
        approve(_certifiedUser, _serielNumber);
        transferFrom(msg.sender,_certifiedUser,_serielNumber);
    }

    

}
