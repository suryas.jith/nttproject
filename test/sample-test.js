const { expect, assert } = require("chai");


describe("Certificate", function () {
  let NTT,ntt,Certificate, certificate, sNum, owner, addr1,addr2,addr3,addr4,totalSupply;

  beforeEach(async () =>{
    
     Certificate = await ethers.getContractFactory('Certificate');
     certificate = await Certificate.deploy();
    
    [owner,addr1,addr2,addr3,addr4] = await ethers.getSigners();
  })
  describe("Certify", async () =>{
    it("Should mint token", async () =>{
      sNum = 11223344
      await certificate.connect(owner).certify(sNum,addr1.address,"qwerty")

    })
    it("Should fail if same serial number given",async () =>{
      sNum = 11223344
      await certificate.connect(owner).certify(sNum,addr1.address,"qwerty")
      totalSupply = 0;
      await expect(certificate.connect(owner).certify(sNum,addr1.address,"qwerty")).to.be.revertedWith('Number already given');

    })
  })
    describe("Checking",async () =>{
      it('Should check every details' ,async () =>{
       
        sNum = 11223344
        await certificate.connect(owner).certify(sNum,addr1.address,"qwerty")
        expect(await certificate.connect(owner).serielNumbers(0)).to.be.eq(sNum)
        expect(await certificate.connect(owner).balanceOf(addr1.address)).to.be.eq(1)
        expect(await certificate.connect(owner).ownerOf(sNum)).to.be.eq(addr1.address)
        // expect(await certificate.connect(owner).tokenURI(sNum)).to.be.eq("qwerty")

        
     

        sNum = 11223345
        await certificate.connect(owner).certify(sNum,addr2.address,"SHA")
        expect(await certificate.connect(owner).serielNumbers(1)).to.be.eq(sNum)
        expect(await certificate.connect(owner).balanceOf(addr2.address)).to.be.eq(1)     
        expect(await certificate.connect(owner).ownerOf(sNum)).to.be.eq(addr2.address)
        // expect(await certificate.connect(owner).tokenURI(sNum)).to.be.eq("SHA")

        

        sNum = 11223346
        await certificate.connect(owner).certify(sNum,addr3.address,"RCP")
        expect(await certificate.connect(owner).serielNumbers(2)).to.be.eq(sNum)
        expect(await certificate.connect(owner).balanceOf(addr3.address)).to.be.eq(1)
        expect(await certificate.connect(owner).ownerOf(sNum)).to.be.eq(addr3.address)
        // expect(await certificate.connect(owner).tokenURI(sNum)).to.be.eq("RCP")

    
        
        sNum = 11223347
        await certificate.connect(owner).certify(sNum,addr4.address,"LSGT")
        expect(await certificate.connect(owner).serielNumbers(3)).to.be.eq(sNum)
        expect(await certificate.connect(owner).balanceOf(addr4.address)).to.be.eq(1)
        expect(await certificate.connect(owner).ownerOf(sNum)).to.be.eq(addr4.address)
      
        

       
        

      })
      it("Should revert if not an issuer",async () => {
          sNum = 11223348
          await certificate.connect(owner).certify(sNum,owner.address,"LSGT1")
          await certificate.connect(owner).issue(addr1.address,sNum);
          await expect(certificate.connect(addr1).issue(owner.address,sNum)).to.be.revertedWith('ERC721: transfer of token not by an authorized issuer');
  
        })
    })
  
});
